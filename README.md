## Andorid Application MyDiary ##

---

The application is a form of a mobile diary. 

It allows you to save notes, download your current location and run an external camera application to take and view a photo from where you are. 
Notes can be modified or deleted, each with runtime information. 
In addition, there is a help view that describes the operation and purpose of the application.

- Software used: Android Studio
- Programming language: Java
